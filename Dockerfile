FROM atlas/analysisbase:21.2.85-centos7

Add . /Tutorial

WORKDIR /Tutorial/build

RUN sudo mkdir ../run

RUN . ~/release_setup.sh && \
    sudo chown -R atlas /Tutorial && \ 
    cmake ../source && \ 
    make  && \ 
    . x86_64-centos7-gcc8-opt/setup.sh && \
    cd ../run

RUN source ~/release_setup.sh && sudo usermod -aG root atlas


